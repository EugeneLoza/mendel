unit CatGenes;

{$mode objfpc}{$H+}

interface

uses
  GeneticsGeneSlotCore, GeneticsGenomeTemplates, GeneticsGeneCore,
  GeneticsCore, GeneticsGeneSlots, GeneticsGenome, GeneticsTexture;

type
  TTailLengthSlot = class(TGeneSlotCore)
    constructor Create; override;
  end;

type
  TCatGenomeTemplate = class(T2GenderGenomeTemplate)
  public
    LeftEyeColorGeneSlot: TEyeColorSlot;
    RightEyeColorGeneSlot: TEyeColorSlot;
    EyeColorAssymetry: TBooleanAssymetryGeneSlot;
    TailLengthGeneSlot: TTailLengthSlot;
    function SayLeftEyeColor(const Owner: TGenome): String;
    function SayRightEyeColor(const Owner: TGenome): String;
    constructor Create; override;
  end;

implementation
uses
  SysUtils,
  CastleUtils;

constructor TTailLengthSlot.Create;
begin
  inherited;
  SlotName := 'Tail length';
  MinValue := 19;
  MaxValue := 32;
  GeneSetLength := 2;
  DominanceType := dtBlend;
end;

constructor TCatGenomeTemplate.Create;
begin
  inherited;
  GenomeTemplateName := 'Cat';
  LeftEyeColorGeneSlot := TEyeColorSlot.Create;
  GeneSlots.Add('LeftEyeColor', LeftEyeColorGeneSlot);
  RightEyeColorGeneSlot := TEyeColorSlot.Create;
  GeneSlots.Add('RightEyeColor', RightEyeColorGeneSlot);
  EyeColorAssymetry := TBooleanAssymetryGeneSlot.Create;
  EyeColorAssymetry.SlotLeft := 'LeftEyeColor';
  EyeColorAssymetry.SlotRight := 'RightEyeColor';
  GeneSlots.Add('Dichromia', EyeColorAssymetry);
  TailLengthGeneSlot := TTailLengthSlot.Create;
  GeneSlots.Add('TailLength', TailLengthGeneSlot);
end;

function TCatGenomeTemplate.SayLeftEyeColor(const Owner: TGenome): String;
begin
  Result := SayLeftAssymetricValue(Owner, 'Dichromia');
end;

function TCatGenomeTemplate.SayRightEyeColor(const Owner: TGenome): String;
begin
  Result := SayRightAssymetricValue(Owner, 'Dichromia');
end;

end.

