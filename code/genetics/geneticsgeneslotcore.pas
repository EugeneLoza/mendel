{ Describes features and methods shared by multiple genes that describe the same property of genome }
unit GeneticsGeneSlotCore;

interface

uses
  Generics.Collections,
  GeneticsCore, GeneticsGeneCore, GeneticsGeneSet;

type
  { A slot to host a Gene(set) determines what type of gene it is
    what are it's basic properties and how does it affect the body }
  TGeneSlotCore = class abstract(TObject)
  public
    { Human-readable name of the gene slot }
    SlotName: String;
    { How this gene is manifested - as Dominant or as Blend }
    DominanceType: TDominanceType;
    { Max and Min values of the gene }
    MaxValue, MinValue: TFloat;
    { Global hash of this slot }
    SlotHash: THash;
    { Length of the gene set in this slot, normally 2 for DNA creatures,
      but exceptions are possible (e.g. gender gene has length of 1) }
    GeneSetLength: Integer;
    { generate a completely random gene in this slot }
    function RandomGene: TGene;
    { generate a completely ransom gene set in this slot }
    function RandomGeneSet(const GenePoll: TGeneList; const Owner: TObject): TGeneSet; //virtual;
    { provide some human-readable string for this gene value }
    function SayValue(const Value: TFloat): String; virtual;
  public
    constructor Create; virtual; //override;
  end;

type
  { to easy find slots by name }
  TGeneSlotDictionary = specialize TObjectDictionary<String, TGeneSlotCore>;

implementation
uses
  SysUtils,
  CastleRandom, GeneticsGeneWrapper;

function TGeneSlotCore.RandomGene: TGene;
begin
  Assert(MaxValue > MinValue);
  Result := TGene.Create;
  Result.GeneSlot := Self;
  Result.Value := Rand.Random * (MaxValue - MinValue) + MinValue;
  if Rand.RandomBoolean then
    Result.Dominance := Rand.Random / 10
  else
    Result.Dominance := 1.0 - Rand.Random / 10;
end;

function TGeneSlotCore.RandomGeneSet(const GenePoll: TGeneList; const Owner: TObject): TGeneSet;
var
  ArrayOfRandomGenes: TArrayOfWrappers;
  I: Integer;
begin
  ArrayOfRandomGenes := nil;
  //SetLength(ArrayOfRandomGenes, 0); //redundant
  for I := 0 to Pred(GeneSetLength) do
  begin
    SetLength(ArrayOfRandomGenes, Length(ArrayOfRandomGenes) + 1);
    ArrayOfRandomGenes[I] := WrapGene(GenePoll[Rand.Random(GenePoll.Count)], Owner);
  end;
  Result := TGeneSet.Create;
  Result.AssignGenes(ArrayOfRandomGenes);
end;

function TGeneSlotCore.SayValue(const Value: TFloat): String;
begin
  Result := FloatToStr(Round(Value * 100) / 100);
end;

constructor TGeneSlotCore.Create;
begin
  inherited Create;
  SlotHash := Rand.Random32Bit;
end;

end.

