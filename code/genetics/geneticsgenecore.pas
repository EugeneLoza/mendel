{ Describes a gene - basic information quant in genetics
  Every gene can have a single float value which manifests differently
  depending on which gene slot this gene is attached to }
unit GeneticsGeneCore;

interface

uses
  Generics.Collections,
  GeneticsCore;

type
  { A gene. Shared by many world inhabitants and usually are passed from
    parents to a child or are chosen from some predefined pool }
  TGene = class(TObject)
  strict private
    FValue: TFloat;
    FDominance: TDominance;
    procedure SetDominance(const AValue: TDominance);
    procedure SetValue(const AValue: TFloat);
    function GetValue: TFloat;
  public
    { is this gene virtual?
      i.e. does not represent a real physocal gene, but rather a 
      calculated result of some sort with the same API }
    IsVirtual: Boolean;
    { To which gene slot does this gene belong }
    GeneSlot: TObject;
    { A random seed unique to this gene }
    GeneHash: THash;
    { How dominant is this gene? }
    property Dominance: TDominance read FDominance write SetDominance;
    { Some value this gene holds }
    property Value: TFloat read GetValue write SetValue;
  public
    constructor Create; //override;
    function DebugString: String; //virtual;
  end;

type
  { Array of genes }
  TArrayOfGenes = array of TGene;
  { generic list of genes }
  TGeneList = specialize TObjectList<TGene>;
  { generic genepoll - all possible genes in different gene slots for a specific genome template }
  TGenePoll = specialize TObjectDictionary<String, TGeneList>;

{ Simple way to create a gene }
function CreateGene(const GeneSlot: TObject; const Value: Single; const Dominance: Single): TGene;
implementation
uses
  SysUtils,
  CastleRandom,
  GeneticsGeneSlotCore;

function CreateGene(const GeneSlot: TObject; const Value: Single; const Dominance: Single): TGene;
begin
  Result := TGene.Create;
  Result.GeneSlot := GeneSlot;
  Result.Value := Value;
  Result.Dominance := Dominance;
end;

{ TGene -----------------------------------------------------------------}

procedure TGene.SetDominance(const AValue: TDominance);
begin
  Assert(AValue >= MinDominance);
  Assert(AValue <= MaxDominance);
  FDominance := AValue;
end;

procedure TGene.SetValue(const AValue: TFloat);
var
  GeneSlotCast: TGeneSlotCore;
begin
  Assert(Assigned(GeneSlot), 'This gene doesn''t have a gene slot assigned');
  GeneSlotCast := GeneSlot as TGeneSlotCore;
  Assert(AValue >= GeneSlotCast.MinValue, 'Received value smaller than MinValue for this gene slot');
  Assert(AValue <= GeneSlotCast.MaxValue, 'Received value larger than MaxValue for this gene slot');
  FValue := AValue;
end;

function TGene.GetValue: TFloat;
begin
  Result := FValue;
end;

function TGene.DebugString: String;
begin
  Result := Self.ClassName;
  Result += '(' + (GeneSlot as TGeneSlotCore).SlotName + ')';
  Result += ': ' + (GeneSlot as TGeneSlotCore).SayValue(Self.Value);
  if not IsVirtual then
    Result += ' [' + DominanceToString(Dominance) + ']'
  else
    Result += ' [Virtual]';
end;

constructor TGene.Create;
begin
  inherited Create;
  GeneHash := Rand.Random32Bit;
end;

end.

