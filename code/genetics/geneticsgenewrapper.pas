{ Wrapper for each gene.
  While genes are property of the World
  Weappers are properties of each separate genome (i.e. specific creature) }
unit GeneticsGeneWrapper;

interface

uses
  GeneticsGeneCore;

type
  { Contains a gene and ownership/manifestation information }
  TGeneWrapper = class(TObject)
  public
    { Genetic data inside }
    Gene: TGene;
    { Current and previous owner of the gene }
    Owner: TObject;
    LastOwner: TObject;
    { Current and previous owner in whom this gene manifested
      i.e. was returned as Value of a gene set.
      nil if not manifested }
    Manifested: TObject;
    LastManifested: TObject;
    { Inform this wrapper that it had manifested }
    procedure Manifest;
    { Return debug information for this gene }
    function DebugString: String;
    { Make a clone of this wrapper }
    function MakeCopy: TGeneWrapper;
    { Make an inherited copy of this wrapper for a new owner (child) }
    function Inherit(const NewOwner: TObject): TGeneWrapper;
  end;

type
  { Array of wrappers, usually inside a single gene set }
  TArrayOfWrappers = array of TGeneWrapper;

{ Wrap a gene for a given owner }
function WrapGene(const Gene: TGene; const Owner: TObject): TGeneWrapper;
implementation
uses
  GeneticsGenome;

function WrapGene(const Gene: TGene; const Owner: TObject): TGeneWrapper;
begin
  Result := TGeneWrapper.Create;
  Result.Gene := Gene;
  Result.Owner := Owner;
end;

{ TGeneWrapper -------------------------------------------------------------}

function TGeneWrapper.MakeCopy: TGeneWrapper;
begin
  Result := TGeneWrapper.Create;
  Result.Gene := Self.Gene;
  Result.Owner := Self.Owner;
  Result.LastOwner := Self.Owner;
  Result.Manifested := Self.Manifested;
  Result.LastManifested := Self.LastManifested;
end;

function TGeneWrapper.Inherit(const NewOwner: TObject): TGeneWrapper;
begin
  Result := TGeneWrapper.Create;
  Result.Gene := Self.Gene;
  Result.LastOwner := Self.Owner;
  Result.Owner := NewOwner;

  Result.LastManifested := Self.LastManifested;
  if Self.Manifested <> nil then
    Result.LastManifested := Self.Manifested;
  Result.Manifested := nil;
end;

procedure TGeneWrapper.Manifest;
begin
  Manifested := Owner;
end;

function TGeneWrapper.DebugString: String;
begin
  Result := Gene.DebugString;
  if LastOwner <> nil then
    Result += ' received from ' + (LastOwner as TGenome).BodyName;
end;

end.

