{ Describes how genes are packed into a single gene slot
  manifests a value based on genetic dominance law in this gene slot
  and blends multiple genes into a new gene set (inheritance)
  also creates "wrappers" for genes to track their affinity to a certain genome }
unit GeneticsGeneSet;

interface

uses
  Generics.Collections,
  GeneticsCore, GeneticsGeneCore, GeneticsGeneWrapper;

type
  { A gcc-complete set of genes (a gene pair in case of DNA)
    each gene set occupies one gene slot }
  TGeneSet = class(TObject)
  strict private
    { Cached value of dominant gene (reference) }
    CachedDominant: TGeneWrapper;
    { Cached value of virtual blend gene (generated) }
    CachedBlend: TGene;
    { Cached value of gene slot (equal for all genes in the set) }
    CachedGeneSlot: TObject;
    { Tests if the genes are blendable }
    procedure AssertGenes;
    { Find a dominant gene (with a wrapper) and cache the value }
    function DominantGene: TGeneWrapper;
    { Calculate genes blend and cache the value
      Warning: it creates a new gene, which must be freed manually }
    function GeneBlend: TGene;
    { Determine and cache gene slot of this gene set }
    function GeneSlot: TObject;
  public
    { Genes in this set, together with their wrappers }
    Genes: TArrayOfWrappers;
    { Length of this gene set (how many genes does it contain, usually 2) }
    function GeneLength: Integer;
    { Calculate value of this gene set considering eominance rules }
    function Value: TGene;
    { Pack several genes into this gene set
      warning: it assumes ownership of the genes,
      if this is unwanted, use MakeCopy/Inherit }
    procedure AssignGenes(const ArrayOfGenes: TArrayOfWrappers);
    { Make a clone of this gene set }
    function DeepCopy: TGeneSet;
  public
    destructor Destroy; override;
    { Mix several gene sets (from parents to child) }
    class function MixGenes(const ArrayOfGeneSets: array of TGeneSet; const Owner: TObject): TGeneSet;
    { Return values stored inside this gene set }
    function DebugString: String;
  end;

type
  { a dictionary of gene sets in their corresponding gene slots
    for quick search and access }
  TGeneSetDictionary = specialize TObjectDictionary<String, TGeneSet>;

implementation
uses
  SysUtils,
  CastleRandom, CastleUtils,
  GeneticsGeneSlotCore, GeneticsGenome;

function TGeneSet.GeneSlot: TObject;
var
  G: TGeneWrapper;
begin
  if CachedGeneSlot = nil then
  begin
    Assert(GeneLength > 0);
    CachedGeneSlot := Genes[0].Gene.GeneSlot;
    for G in Genes do
      Assert(CachedGeneSlot = G.Gene.GeneSlot);
  end;
  Result := CachedGeneSlot;
end;

function TGeneSet.DominantGene: TGeneWrapper;
var
  G: TGeneWrapper;
  D: TDominance;
  A: TArrayOfWrappers;
begin
  if CachedDominant = nil then
  begin
    AssertGenes;
    Assert(GeneLength > 0);
    D := Genes[0].Gene.Dominance;
    A := nil;
    for G in Genes do
      if G.Gene.Dominance > D then
      begin
        SetLength(A, 1);
        D := G.Gene.Dominance;
        A[0] := G;
      end else
      if G.Gene.Dominance = D then
      begin
        SetLength(A, Length(A) + 1);
        A[Pred(Length(A))] := G;
      end;
    G := A[Rand.Random(Length(A))];
    G.Manifest;
    CachedDominant := G;
  end;
  Result := CachedDominant;
end;

function TGeneSet.GeneBlend: TGene;
var
  G: TGeneWRapper;
  D: TDominance;
  V: TFloat;
begin
  if CachedBlend = nil then
  begin
    AssertGenes;
    D := 0;
    for G in Genes do
      D += Sqr(G.Gene.Dominance + 1); // this will result in 1:4 versus Mendel's 1:3
    V := 0;
    for G in Genes do
      V += G.Gene.Value * Sqr(G.Gene.Dominance + 1);
    V := V / D;
    CachedBlend := TGene.Create;
    CachedBlend.GeneSlot := GeneSlot;
    CachedBlend.IsVirtual := true;
    CachedBlend.Dominance := 0;
    CachedBlend.Value := V;
  end;
  Result := CachedBlend;
end;

function TGeneSet.Value: TGene;
begin
  case TGeneSlotCore(GeneSlot).DominanceType of
    dtDominant: Result := DominantGene.Gene;
    dtBlend: Result := GeneBlend;
    else
      raise EInternalError.Create('Unknown DominanceType!');
  end;
end;

procedure TGeneSet.AssertGenes;
var
  S: String;
  G: TGeneWrapper;
begin
  //Assert(Body <> nil, 'Gene set requires a heart to manifest');
  Assert(Length(Genes) > 0, 'Gene set must contain at least one gene!');
  S := Genes[0].Gene.ClassName;
  for G in Genes do
    Assert(S = G.Gene.ClassName, 'Some of genes in the gene set are not compatible');
end;

function TGeneSet.GeneLength: Integer;
begin
  Result := Length(Genes);
end;

class function TGeneSet.MixGenes(const ArrayOfGeneSets: array of TGeneSet; const Owner: TObject): TGeneSet;
var
  NewGenesSet: TArrayOfWrappers;
  NewGenesLength: Integer;
  function AssertGenesLength: Integer;
  var
    I: Integer;
  begin
    Result := ArrayOfGeneSets[0].GeneLength;
    Assert(Result > 0, 'MixGenes received a zero-length gene set');
    for I := Low(ArrayOfGeneSets) to High(ArrayOfGeneSets) do
      Assert(Result = ArrayOfGeneSets[I].GeneLength, 'Unable to blend genes of different lenth');
  end;
  procedure ShuffleGenes(var GenesArray: TArrayOfWrappers);
  var
    K, R: Integer;
    G: TGeneWrapper;
  begin
    for K := Low(GenesArray) to High(GenesArray) do
    begin
      R := Rand.Random(Length(GenesArray));
      G := GenesArray[K];
      GenesArray[K] := GenesArray[R];
      GenesArray[R] := G;
    end;
  end;
  function AssertGeneSlot: TObject;
  var
    I: Integer;
  begin
    Assert(Length(ArrayOfGeneSets) > 0);
    Result := ArrayOfGeneSets[0].GeneSlot;
    for I := Low(ArrayOfGeneSets) to High(ArrayOfGeneSets) do
      Assert(Result = ArrayOfGeneSets[I].GeneSlot);
  end;
var
  I, J, N: Integer;
begin
  NewGenesLength := AssertGenesLength;
  NewGenesSet := nil;
  SetLength(NewGenesSet, ArrayOfGeneSets[0].GeneLength * Length(ArrayOfGeneSets));
  //shouldn't body handle mixing of genes to get access to hash and etc?
  //let it be totally random for now
  N := 0;
  for I := Low(ArrayOfGeneSets) to High(ArrayOfGeneSets) do
    for J := Low(ArrayOfGeneSets[I].Genes) to High(ArrayOfGeneSets[I].Genes) do
    begin
      NewGenesSet[N] := ArrayOfGeneSets[I].Genes[J];
      Inc(N);
    end;
  Assert(N = Length(NewGenesSet), 'Amount of genes added does not correspond to NewGenesSet length');
  ShuffleGenes(NewGenesSet);
  SetLength(NewGenesSet, NewGenesLength); // This will crop the shuffled array to NewGenesLength first values
  //and create copies of genes before passing them to AssignGenes.//maybe, we should copy them in AssignGenes?
  for I := Low(NewGenesSet) to High(NewGenesSet) do
    NewGenesSet[I] := NewGenesSet[I].Inherit(Owner);
  Result := TGeneSet.Create;
  Result.AssignGenes(NewGenesSet);
  Result.AssertGenes;
  Assert(Result.GeneLength = NewGenesLength);

  Result.DominantGene; //todo?
end;

procedure TGeneSet.AssignGenes(const ArrayOfGenes: TArrayOfWrappers);
var
  I: Integer;
begin
  SetLength(Genes, Length(ArrayOfGenes));
  for I := Low(ArrayOfGenes) to High(ArrayOfGenes) do
    Genes[I] := ArrayOfGenes[I];
  AssertGenes;
  DominantGene; //todo?
end;

function TGeneSet.DeepCopy: TGeneSet;
var
  I: Integer;
begin
  Result := TGeneSet.Create;
  SetLength(Result.Genes, Length(Self.Genes));
  for I := Low(Genes) to High(Genes) do
    Result.Genes[I] := Self.Genes[I].MakeCopy;
end;

destructor TGeneSet.Destroy;
var
  I: Integer;
begin
  for I := Low(Genes) to High(Genes) do
    FreeAndNil(Genes[I]);
  FreeAndNil(CachedBlend);
  inherited Destroy;
end;

function TGeneSet.DebugString: String;
var
  I: Integer;
begin
  Result := Self.ClassName + ':' + NL;
  for I := Low(Genes) to High(Genes) do
    Result := Result + IntToStr(I) + ' = ' + Genes[I].DebugString + NL;
  Result := Result + 'M = ' + Value.DebugString;
  if DominantGene.LastManifested <> nil then
    Result += ' just like ' + (DominantGene.LastManifested as TGenome).BodyName
  else
    Result += ' never seen in the family before.'
end;

end.

