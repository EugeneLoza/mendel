{ Often-used genome templates }
unit GeneticsGenomeTemplates;

interface

uses
  GeneticsCore,
  GeneticsGenomeTemplateCore, GeneticsGeneSlots, GeneticsGenome, GeneticsGeneCore;

type
  { Adds helpers for TAbstractAssymetryGeneSlot to TAbstractGenomeTemplate
    suggests that assymetry can be only between Left&Right values of some gene }
  TAssymetricGenomeTemplate = class(TAbstractGenomeTemplate)
  public
    { Gets "Left" Value of Left&Right gene sets considering assymetry }
    function GetLeftAssymetricValue(const Owner: TGenome; const GeneSlotName: String): TFloat;
    { Gets "Right" Value of Left&Right gene sets considering assymetry }
    function GetRightAssymetricValue(const Owner: TGenome; const GeneSlotName: String): TFloat;
    { Same for debug purposes }
    function SayLeftAssymetricValue(const Owner: TGenome; const GeneSlotName: String): String;
    function SayRightAssymetricValue(const Owner: TGenome; const GeneSlotName: String): String;
  end;

type
  { A core template for two-gender species }
  T2GenderGenomeTemplate = class(TAssymetricGenomeTemplate)
  public
    GenderGeneSlot: T2GenderGeneSlot;
    function RandomMaleGenome(const GenePoll: TGenePoll): TGenome;
    function RandomFemaleGenome(const GenePoll: TGenePoll): TGenome;
    class function MaleChildGenome(const ArrayOfBodies: TArrayOfGenomes): TGenome;
    class function FemaleChildGenome(const ArrayOfBodies: TArrayOfGenomes): TGenome;
    constructor Create; override;
  end;

implementation
uses
  SysUtils,
  GeneticsGeneSlotCore;

function TAssymetricGenomeTemplate.GetLeftAssymetricValue(const Owner: TGenome; const GeneSlotName: String): TFloat;
var
  AssymetricSlot: TAbstractAssymetryGeneSlot;
begin
  AssymetricSlot := GeneSlots[GeneSlotName] as TAbstractAssymetryGeneSlot;
  Result := AssymetricSlot.LeftValue(
    Owner.GeneSets[GeneSlotName].Value.Value,
    Owner.GeneSets[AssymetricSlot.SlotLeft],
    Owner.GeneSets[AssymetricSlot.SlotRight]
    );
end;

function TAssymetricGenomeTemplate.GetRightAssymetricValue(const Owner: TGenome; const GeneSlotName: String): TFloat;
var
  AssymetricSlot: TAbstractAssymetryGeneSlot;
begin
  AssymetricSlot := GeneSlots[GeneSlotName] as TAbstractAssymetryGeneSlot;
  Result := AssymetricSlot.RightValue(
    Owner.GeneSets[GeneSlotName].Value.Value,
    Owner.GeneSets[AssymetricSlot.SlotLeft],
    Owner.GeneSets[AssymetricSlot.SlotRight]
    );
end;

function TAssymetricGenomeTemplate.SayLeftAssymetricValue(const Owner: TGenome; const GeneSlotName: String): String;
begin
  Result :=
    GeneSlots[(GeneSlots[GeneSlotName] as TAbstractAssymetryGeneSlot).SlotLeft].SayValue(
    GetLeftAssymetricValue(Owner, GeneSlotName));
end;

function TAssymetricGenomeTemplate.SayRightAssymetricValue(const Owner: TGenome; const GeneSlotName: String): String;
begin
  Result :=
    GeneSlots[(GeneSlots[GeneSlotName] as TAbstractAssymetryGeneSlot).SlotRight].SayValue(
    GetRightAssymetricValue(Owner, GeneSlotName));
end;

class function T2GenderGenomeTemplate.MaleChildGenome(const ArrayOfBodies: TArrayOfGenomes): TGenome;
begin
  Result := nil;
  repeat
    FreeAndNil(Result);
    Result := ChildGenome(ArrayOfBodies);
  until Round(Result.GeneSets['Gender'].Value.Value) = gtMale;
end;

class function T2GenderGenomeTemplate.FemaleChildGenome(const ArrayOfBodies: TArrayOfGenomes): TGenome;
begin
  Result := nil;
  repeat
    FreeAndNil(Result);
    Result := ChildGenome(ArrayOfBodies);
  until Round(Result.GeneSets['Gender'].Value.Value) = gtFemale;
end;

function T2GenderGenomeTemplate.RandomMaleGenome(const GenePoll: TGenePoll): TGenome;
begin
  Result := nil;
  repeat
    FreeAndNil(Result);
    Result := RandomGenome(GenePoll);
  until Round(Result.GeneSets['Gender'].Value.Value) = gtMale;
end;

function T2GenderGenomeTemplate.RandomFemaleGenome(const GenePoll: TGenePoll): TGenome;
begin
  Result := nil;
  repeat
    FreeAndNil(Result);
    Result := RandomGenome(GenePoll);
  until Round(Result.GeneSets['Gender'].Value.Value) = gtFemale;
end;

constructor T2GenderGenomeTemplate.Create;
begin
  inherited Create;
  GenderGeneSlot := T2GenderGeneSlot.Create;
  GeneSlots.Add('Gender', GenderGeneSlot);
end;


end.

