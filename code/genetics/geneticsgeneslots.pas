{ Often-used gene slots }
unit GeneticsGeneSlots;

interface

uses
  Classes,
  CastleColors, CastleImages,
  GeneticsCore, GeneticsGeneCore, GeneticsGeneSlotCore,  GeneticsGeneSet;

const
  { Gender constants }
  gtMale = 0;
  gtFemale = 1;

type
  { A slot to contain male or female gender gene }
  T2GenderGeneSlot = class(TGeneSlotCore)
  public
    function SayValue(const Value: TFloat): String; override;
    constructor Create; override;
  end;

type
  { A yes/no gene }
  TBooleanSlot = class(TGeneSlotCore)
  public
    { Return boolean for a value }
    function BooleanValue(const AValue: TFloat): Boolean;
    constructor Create; override;
  end;

type
  { A possible eye color function derieved from multiple
   eye color charts found over the internet.
   It doesn't correspond to any real-life creature,
   just trying to look a bit more natural than random color. }
  TEyeColorSlot = class(TGeneSlotCore)
  strict protected
    const
      ecDark   : TCastleColor = (Data: ( 0.3 , 0.3 , 0.3 , 1.0));
      ecGray   : TCastleColor = (Data: ( 0.9 , 0.9 , 0.9 , 1.0));
      ecPurple : TCastleColor = (Data: ( 0.5 , 0.3 , 0.5 , 1.0));
      ecRed    : TCastleColor = (Data: ( 0.8 , 0.5 , 0.5 , 1.0));
      ecBrown  : TCastleColor = (Data: ( 0.5 , 0.3 , 0.2 , 1.0));
      ecGold   : TCastleColor = (Data: ( 0.8 , 0.8 , 0.5 , 1.0));
      ecGreen  : TCastleColor = (Data: ( 0.2 , 0.8 , 0.5 , 1.0));
      ecCyan   : TCastleColor = (Data: ( 0.2 , 0.8 , 0.8 , 1.0));
      ecBlue   : TCastleColor = (Data: ( 0.2 , 0.4 , 0.8 , 1.0));
      ecWhite  : TCastleColor = (Data: ( 0.9 , 0.9 , 0.9 , 1.0));
  public
    function SayValue(const Value: TFloat): String; override;
    { Value of this gene as TCastleColor }
    class function Color(const Value: TFloat): TCastleColor;
    constructor Create; override;
  end;
  
type
  { A gene slot converting a Value into a String }
  TStringSlot = class(TGeneSlotCore)
  strict private
    FStrings: TStringList;
  public
    { Adds a string to the list }
    procedure Add(const AString: String);
    { return string value for the given foiat value }
    function StringValue(const AValue: TFloat): String;
    constructor Create; override;
    destructor Destroy; override;
  end;

type
  { a basic gene slot for texture URL,
    uses texture cache to load textures }
  TTextureSlot = class(TStringSlot)
  public
    { Return (cached) texture image for the given value }
    function Texture(const AValue: TFloat): TCastleImage;
  end;

{hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh}
{ I don't like how assymetry textures work, maybe, not use them at all and do it manually in texture blend3r?
  move texture to GeneticsTextures }
{type
  TAbstractAssymetricTexture = class abstract(TGeneSlotCore)
  public
    //TextureSlot: TTextureSlot;
    function AssymetricTexture(const AValue: TFloat; const TextureGeneSet: TGeneSet): TCastleImage; virtual; abstract;
    constructor Create; override;
  end;
  
type
  TBooleanAssymetricTexture = class(TAbstractAssymetricTexture)
  public
    Threshold: Single;
    function AssymetricTexture(const AValue: TFloat; const TextureGeneSet: TGeneSet): TCastleImage; override;
    constructor Create; override;
  end;
  
type
  TFloatAssymetricTexture = class(TAbstractAssymetricTexture)
  public
    function AssymetricTexture(const AValue: TFloat; const TextureGeneSet: TGeneSet): TCastleImage; override;
    constructor Create; override;
  end;}
{hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh}
  
type
  { Abstract routines used by boolean and float assymetry genes }
  TAbstractAssymetryGeneSlot = class abstract(TGeneSlotCore)
  public
    { Names of left snd right slots this gene operates }
    SlotLeft, SlotRight: String;
    { Get correspondingly left and right values considering assymetry }
    function LeftValue(const AssymetryValue: TFloat; const GeneSetLeft, GeneSetRight: TGeneSet): TFloat; virtual; abstract;
    function RightValue(const AssymetryValue: TFloat; const GeneSetLeft, GeneSetRight: TGeneSet): TFloat; virtual; abstract;
    constructor Create; override;
  end;

type
  { Assymetry that works like on/off,
    good example would be cat eyes heterochromia
    the assymetry chance is defined by Threshold value -
    if the Value is above threshold, then assymetry manifests }
  TBooleanAssymetryGeneSlot = class(TAbstractAssymetryGeneSlot)
  public
    { Assymetry switches on if Value is gearter than this value
      default 0.9 }
    Threshold: TFloat;
    function SayValue(const Value: TFloat): String; override;
    function LeftValue(const AssymetryValue: TFloat; const GeneSetLeft, GeneSetRight: TGeneSet): TFloat; override;
    function RightValue(const AssymetryValue: TFloat; const GeneSetLeft, GeneSetRight: TGeneSet): TFloat; override;
    constructor Create; override;
  end;

type
  { Float assymetry,
    a good example of such assymetry will be scale of left and right bodyparts
    be sure to submit small values to avoid weird results }
  TFloatAssymetryGeneSlot = class(TAbstractAssymetryGeneSlot)
  public
    function LeftValue(const AssymetryValue: TFloat; const GeneSetLeft, GeneSetRight: TGeneSet): TFloat; override;
    function RightValue(const AssymetryValue: TFloat; const GeneSetLeft, GeneSetRight: TGeneSet): TFloat; override;
    constructor Create; override;
  end;

{ Load and cache a texture }
function GetTexture(const ATextureUrl: String): TCastleImage;
implementation
uses
  Generics.Collections, Generics.Defaults,
  CastleUtils;
  
type
  TTextureCache = specialize TObjectDictionary<String, TCastleImage>;
  
var
  TextureCache: TTextureCache;
  
function GetTexture(const ATextureUrl: String): TCastleImage;
begin
  if not TextureCache.TryGetValue(ATextureUrl, Result) then
  begin
    Result := LoadImage(ATextureUrl);
    TextureCache.Add(ATextureUrl, Result);
  end;
end;
  
{ T2GenderGeneSlot -------------------------------------------- }

function T2GenderGeneSlot.SayValue(const Value: TFloat): String;
begin
  //inherited
  case Round(Value) of
    gtMale: Result := 'Male';
    gtFemale: Result := 'Female';
    else
      raise EInternalError.Create('Unknown gender');
  end;
end;

constructor T2GenderGeneSlot.Create;
begin
  inherited Create;
  SlotName := 'Gender';
  MinValue := gtMale;
  MaxValue := gtFemale;
  GeneSetLength := 1; //only one gene in the set
end;

{ TBooleanSlot ------------------------------------------------ }

function TBooleanSlot.BooleanValue(const AValue: TFloat): Boolean;
begin
  if AValue < 0.5 then
    Result := true
  else
    Result := false;
end;

constructor TBooleanSlot.Create;
begin
  inherited Create;
  SlotName := 'Boolean';
  MinValue := 0;
  MaxValue := 1;
  GeneSetLength := 2;
  DominanceType := dtDominant;
end;

{ TEyeColorSlot ------------------------------------------------ }

constructor TEyeColorSlot.Create;
begin
  inherited Create;
  SlotName := 'Eye color';
  MinValue := 0;
  MaxValue := 9;
  GeneSetLength := 2;
  DominanceType := dtDominant;
end;

function TEyeColorSlot.SayValue(const Value: TFloat): String;
begin
  //inherited;
  case Round(Value) of
    0: Result := 'Dark';
    1: Result := 'Gray';
    2: Result := 'Purple';
    3: Result := 'Red';
    4: Result := 'Brown';
    5: Result := 'Golden';
    6: Result := 'Dark green';
    7: Result := 'Green';
    8: Result := 'Blue';
    9: Result := 'White';
    else
      raise EInternalError.Create('Unknown eye color');
  end;
end;

class function TEyeColorSlot.Color(const Value: TFloat): TCastleColor;
begin
  if Value < 1 then
    Result := TCastleColor.Lerp(Value - 0, ecDark, ecGray)
  else
  if Value < 2 then
    Result := TCastleColor.Lerp(Value - 1, ecGray, ecPurple)
  else
  if Value < 3 then
    Result := TCastleColor.Lerp(Value - 2, ecPurple, ecRed)
  else
  if Value < 4 then
    Result := TCastleColor.Lerp(Value - 3, ecRed, ecBrown)
  else
  if Value < 5 then
    Result := TCastleColor.Lerp(Value - 4, ecBrown, ecGold)
  else
  if Value < 6 then
    Result := TCastleColor.Lerp(Value - 5, ecGold, ecGreen)
  else
  if Value < 7 then
    Result := TCastleColor.Lerp(Value - 6, ecGreen, ecCyan)
  else
  if Value < 8 then
    Result := TCastleColor.Lerp(Value - 7, ecCyan, ecBlue)
  else
  if Value <= 9 then
    Result := TCastleColor.Lerp(Value - 8, ecBlue, ecGray)
  else
    raise EInternalError.Create('Unknown eye color');
end;

{ TStringSlot --------------------------------------------- }

procedure TStringSlot.Add(const AString: String);
begin
  FStrings.Add(AString);
  MaxValue := FStrings.Count;
end;

function TStringSlot.StringValue(const AValue: TFloat): String;
begin
  Assert(AValue < FStrings.Count);
  Assert(FStrings.Count > 0);
  Result := FStrings[Trunc(AValue)];
end;

constructor TStringSlot.Create;
begin
  inherited Create;
  SlotName := 'String';
  FStrings := TStringList.Create;
  MinValue := 0;
  MaxValue := 0;
  GeneSetLength := 2;
  DominanceType := dtDominant;
end;

destructor TStringSlot.Destroy;
begin
  FStrings.Free;
  inherited Destroy;
end;

{ TTextureSlot -------------------------------------------- }

function TTextureSlot.Texture(const AValue: TFloat): TCastleImage;
begin
  Result := GetTexture(StringValue(AValue));
end;

{hhhhhhhhhhhhhh}
{ TAbstractAssymetricTexture -------------------------------- }

{constructor TAbstractAssymetryTextureSlot.Create;
begin
  inherited Create;
  MinValue := 0;
  MaxValue := 1;
  GeneSetLength := 2;
end;

{ TBooleanAssymetryTextureSlot -------------------------------- }

constructor TBooleanAssymetryTextureSlot.Create;
begin
  inherited Create;
  SlotName := 'Boolean assymetric texture';
  Threshold := 0.9;
  DominanceType := dtDominant;
end;

function TBooleanAssymetryTextureSlot.AssymetricTexture(const AValue: TFloat; const TextureGeneSet: TGeneSet): TCastleImage;
begin
  if AValue < Threshold then
    Result := TextureGeneSlot(TextureGeneSet.Value)?
  else
    Result := Texture1 / 2 + Texture2 / 2;
end;

{ TFloatAssymetryTextureSlot -------------------------------- }

constructor TFloatAssymetryTextureSlot.Create;
begin
  inherited Create;
  SlotName := 'Float assymetric texture';
  DominanceType := dtBlend;
end;

function TFloatAssymetryTextureSlot.AssymetricTexture(const AValue: TFloat; const TextureGeneSet: TGeneSet): TCastleImage;
begin
  Result := ...?
end;}

{ TAbstractAssymetryGeneSlot -------------------------------- }
{hhhhhhhhhhhhhh}

constructor TAbstractAssymetryGeneSlot.Create;
begin
  inherited Create;
  MinValue := 0;
  MaxValue := 1;
  GeneSetLength := 2;
end;

{ TBooleanAssymetryGeneSlot -------------------------------- }

constructor TBooleanAssymetryGeneSlot.Create;
begin
  inherited Create;
  SlotName := 'Boolean assymetry';
  Threshold := 0.9;
  DominanceType := dtDominant;
end;

function TBooleanAssymetryGeneSlot.LeftValue(const AssymetryValue: TFloat; const GeneSetLeft, GeneSetRight: TGeneSet): TFloat;
begin
  if AssymetryValue < Threshold then
  begin
    if GeneSetLeft.Value.Dominance > GeneSetRight.Value.Dominance then
      Result := GeneSetLeft.Value.Value
    else
      Result := GeneSetRight.Value.Value;
  end else
    Result := GeneSetLeft.Value.Value;
end;

function TBooleanAssymetryGeneSlot.RightValue(const AssymetryValue: TFloat; const GeneSetLeft, GeneSetRight: TGeneSet): TFloat;
begin
  if AssymetryValue < Threshold then
  begin
    if GeneSetLeft.Value.Dominance > GeneSetRight.Value.Dominance then
      Result := GeneSetLeft.Value.Value
    else
      Result := GeneSetRight.Value.Value;
  end else
    Result := GeneSetRight.Value.Value;
end;

function TBooleanAssymetryGeneSlot.SayValue(const Value: TFloat): String;
begin
  if Value > Threshold then
    Result := 'Assymetric'
  else
    Result := 'Symmetric';
end;

{ TFloatAssymetryGeneSlot -------------------------------- }

constructor TFloatAssymetryGeneSlot.Create;
begin
  inherited Create;
  SlotName := 'Float assymetry';
  DominanceType := dtBlend;
end;

function TFloatAssymetryGeneSlot.LeftValue(const AssymetryValue: TFloat; const GeneSetLeft, GeneSetRight: TGeneSet): TFloat;
begin
  Result := lerp(AssymetryValue, GeneSetLeft.Value.Value, GeneSetRight.Value.Value);
end;

function TFloatAssymetryGeneSlot.RightValue(const AssymetryValue: TFloat; const GeneSetLeft, GeneSetRight: TGeneSet): TFloat;
begin
  Result := lerp(AssymetryValue, GeneSetRight.Value.Value, GeneSetLeft.Value.Value);
end;

initialization
  TextureCache := TTextureCache.Create([doOwnsValues]);

finalization
  TextureCache.Free;
  
end.

