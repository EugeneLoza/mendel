{ Genome template is identical for all bodies of a specific kind
  it contains information on gene slots in the Genome and means
  to work with it, e.g. to create a random Genome or a child.
  Every Genome template is a species. }
unit GeneticsGenomeTemplateCore;

interface

uses
  GeneticsGeneCore, GeneticsGeneSlotCore, GeneticsGenome;

type
  { Core of the Genome template, doesn't know anything about the genome
    should be overridden to use for specific genomes }
  TAbstractGenomeTemplate = class(TObject)
  public
    { Name of the species }
    GenomeTemplateName: String;
    { Gene slots in this template }
    GeneSlots: TGeneSlotDictionary;
    { Create a random genome from a gene poll }
    function RandomGenome(const GenePoll: TGenePoll): TGenome;
    { Create a child from a set of parent genomes }
    class function ChildGenome(const ArrayOfBodies: TArrayOfGenomes): TGenome;
  public
    constructor Create; virtual; //override;
    destructor Destroy; override;
  end;

implementation
uses
  SysUtils, Generics.Collections,
  GeneticsGeneSet;

function TAbstractGenomeTemplate.RandomGenome(const GenePoll: TGenePoll): TGenome;
var
  S: String;
  function AssertGenomeTemplate: TAbstractGenomeTemplate;
  begin
    Result := Self;
  end;
begin
  Result := TGenome.Create;
  Result.GenomeTemplate := AssertGenomeTemplate;
  Assert(GeneSlots.Count = GenePoll.Count);
  for S in GeneSlots.Keys do
    Result.GeneSets.Add(S, GeneSlots[S].RandomGeneSet(GenePoll[S], Result));
    //assert GenePoll.HasKey(S)?
end;

class function TAbstractGenomeTemplate.ChildGenome(const ArrayOfBodies: TArrayOfGenomes): TGenome;
  function AssertGenomeTemplate: TAbstractGenomeTemplate;
  var
    K: Integer;
  begin
    Result := ArrayOfBodies[0].GenomeTemplate as TAbstractGenomeTemplate;
    for K := Low(ArrayOfBodies) to High(ArrayOfBodies) do
      Assert(Result = ArrayOfBodies[K].GenomeTemplate);
  end;
var
  I: Integer;
  S: String;
  GeneSetArray: array of TGeneSet;
begin
  Assert(Length(ArrayOfBodies) > 0);
  Result := TGenome.Create;
  Result.GenomeTemplate := AssertGenomeTemplate;

  GeneSetArray := nil;
  SetLength(GeneSetArray, Length(ArrayOfBodies));
  for S in ArrayOfBodies[0].GeneSets.Keys do
  begin
    for I := Low(ArrayOfBodies) to High(ArrayOfBodies) do
      GeneSetArray[I] := ArrayOfBodies[I].GeneSets[S];
    Result.GeneSets.Add(S, TGeneSet.MixGenes(GeneSetArray, Result));
  end;
end;

constructor TAbstractGenomeTemplate.Create;
begin
  inherited Create;
  GeneSlots := TGeneSlotDictionary.Create([doOwnsValues]);
end;

destructor TAbstractGenomeTemplate.Destroy;
begin
  FreeAndNil(GeneSlots);
  inherited Destroy;
end;

end.

