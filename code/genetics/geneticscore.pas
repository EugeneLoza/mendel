{ Basic constants and types for genetics }
unit GeneticsCore;

interface

const
  { max/min Dominance values;
    pay attention, these values are sometimes hardcoded }
  MinDominance = 0.0;
  MaxDominance = 1.0;

type
  { Float value }
  TFloat = Single;
  { Dominance type }
  TDominance = Single;
  { Hash of different genetics features }
  THash = LongWord;

type
  { How a gene blends with other genes in a gene set }
  TDominanceType = (
    { The most dominant gene is the one manifested }
    dtDominant,
    { The genes blend smoothly with result determined more by
      dominant gene(s), but also (to lesser extent) by recessive gene(s) }
    dtBlend);

function DominanceToString(const ADominance: TDominance): String;
implementation
uses
  CastleUtils;

function DominanceToString(const ADominance: TDominance): String;
begin
  if ADominance > 0.5 then
    Result := 'Dominant'
  else
    Result := 'Recessive';
end;

end.

