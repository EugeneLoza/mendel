{ Body genome - a complete representation of genes inside a specific creature }
unit GeneticsGenome;

interface

uses
  GeneticsCore, GeneticsGeneSet;

type
  {}
  TGenome = class(TObject)
  public
    { Name of this genome (proper name )}
    BodyName: String;
    { hash of this genome }
    GenomeHash: THash;
    { Template of this genome}
    GenomeTemplate: TObject;
    { Gene sets in the gene slots for this genome c}
    GeneSets: TGeneSetDictionary;
    { make a clone of this genome }
    function DeepCopy: TGenome;
  public
    constructor Create; virtual; //override;
    destructor Destroy; override;
    function DebugString: String; virtual;
  end;
  TGenomeClass = class of TGenome;

type
  TArrayOfGenomes = array of TGenome;

implementation
uses
  SysUtils, Generics.Collections,
  CastleRandom, CastleUtils,
  GeneticsGenomeTemplateCore;

function TGenome.DeepCopy: TGenome;
var
  S: String;
begin
  Result := TGenomeClass(Self.ClassType).Create;
  Result.BodyName := Self.BodyName + ' clone';
  Result.GenomeTemplate := Self.GenomeTemplate;
  for S in GeneSets.Keys do
    Result.GeneSets.Add(S, Self.GeneSets[S].DeepCopy);
end;

constructor TGenome.Create;
begin
  inherited Create;
  GenomeHash := Rand.Random32Bit;
  GeneSets := TGeneSetDictionary.Create([doOwnsValues]);
end;

destructor TGenome.Destroy;
begin
  FreeAndNil(GeneSets);
  inherited Destroy;
end;

function TGenome.DebugString: String;
var
  S: String;
begin
  Result := Self.ClassName + '(' + Self.BodyName + '/' +
    (GenomeTemplate as TAbstractGenomeTemplate).GenomeTemplateName + '):' + NL;
  for S in GeneSets.Keys do
    Result := Result + GeneSets[S].DebugString + NL;
end;

end.

