{}
unit GeneticsTexture;

interface
uses
  CastleImages, CastleColors;
  
type
  {}
  TGeneticTexture = class(TRgbAlphaImage)
  public
    {}
    procedure DrawFromColorized(const ASource: TRgbAlphaImage;
      const AColor: TCastleColor; const ANoise: Single = 0.0);
  end;

implementation
uses
  CastleVectors, CastleRandom;

procedure TGeneticTexture.DrawFromColorized(const ASource: TRgbAlphaImage;
  const AColor: TCastleColor; const ANoise: Single = 0.0);
  function BlendByte(const AValue: Single): Byte; inline;
  begin
    if AValue > 255 then
      Result := 255
    else
      Result := Trunc(AValue);
  end;
var
  DestX, DestY: Integer;
  PSource, PDest: PVector4Byte;
  Alpha1, Alpha2, Alpha1d, AlphaSum: Single;
  Noise: Single;
begin
  Assert(Self.Width = ASource.Width);
  Assert(Self.Height = ASource.Height);

  for DestY := 0 to Pred(Height) do
  begin
    PSource := ASource.PixelPtr(0, DestY);
    PDest := PixelPtr(0, DestY);
    for DestX := 0 to Pred(Width) do
    begin
      Alpha1 := PDest^.Data[3] / 255;
      Alpha2 := PSource^.Data[3] / 255;
      // calculate alpha-sums according to https://en.wikipedia.org/wiki/Alpha_compositing
      Alpha1d := Alpha1 * (1 - Alpha2);
      AlphaSum := Alpha1 + (1 - Alpha1) * Alpha2;
      if AlphaSum > 0 then
      begin
        Noise := ANoise * Rand.Random;
        PDest^.Data[0] := BlendByte(AColor.Data[0] * (PDest^.Data[0] * Alpha1d + PSource^.Data[0] * Alpha2) / AlphaSum + Noise);
        PDest^.Data[1] := BlendByte(AColor.Data[1] * (PDest^.Data[1] * Alpha1d + PSource^.Data[1] * Alpha2) / AlphaSum + Noise);
        PDest^.Data[2] := BlendByte(AColor.Data[2] * (PDest^.Data[2] * Alpha1d + PSource^.Data[2] * Alpha2) / AlphaSum + Noise);
        PDest^.Data[3] := BlendByte(AColor.Data[3] * 256 * AlphaSum);
      end;
      Inc(PSource);
      Inc(PDest);
    end;
  end;
end;

end.
