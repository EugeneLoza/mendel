BaseTexture: TTextureSlot;
BasePattern: TTextureSlot;
SpotsPattern: TTextureSlot;
PrimaryFurColorSlot: TFurColorSlot;
SecondaryFurColorSlot: TFurColorSlot;
TretiaryFurColorSlot: TFurColorSlot;
function SupplementaryFurColor: TCastleColor;
{ all flips in texture must be handles as separate images }

{sphynx get different body and different (correlation with heart)
 base texture set, don't try to do everything at once
 what if both texture do not fit the heart? or to simplify,
 let heart just force body and base textrue, ignoring genes
 (i.e. fur length and curls are obviously ignored, but passed on normally)}

function TCatGenome.BlendTexture: TGeneticTexture;
var
  Textures: array [0..2] of TCastleImage;
  Eyes: array [0..2] of TCastleImage; //make eyes separate!
begin
  if FTexture = nil then
  begin
    //Correlation of pattern with heart (or blending method?)
    Result := TGeneticTexture.Create(BaseTexture.Width, BaseTexture.Height);
    Result.DrawFromColorized(BaseRexture, FurPrimaryColorGeneSet.Color);
    Result.AddNoise(TextureNoiseGeneSet.Value);
    //base texture must apply to color texture, but not eyes, etc
    //blending method: overlay, multiply

    if SecondaryPatternBoolean.ValueBoolean then
      for I := 0 to SecondaryTextureSlots do
        if SecondaryTextureBoolean[I] then
          Result.DrawFromColorized(SecondaryFurPattern[I].Texture, SecondaryFurColorSlot.Color);
          // need to keep transparency!
    if TretiaryPatternBoolean.ValueBoolean then
      for I := 0 to TretiaryTextureSlots do
        if TretiaryTextureBoolean[I] then
          Result.DrawFromColorized(TretiaryFurPattern[I].Texture, TretiaryFurColorSlot.Color);

    Result.DrawFromColorized(LeftEyeTexture.Texture(EyesTextureGeneSet.Texture), LeftEyeColorGeneSet.Color);
    Eyes[1] := RightEyeTexture.Texture(EyesTextureGeneSet.Texture).DeepCopy;
    Eyes[1].Colorize(RightEyeColorGeneSet.Color);
  
    Skin := SkinTexture.Texture(SkinTextureGeneSet.Texture).DeepCopy;
    Skin.Colorize(SkinColorGeneSet.Color);
  
    Nose := SkinTexture.Texture(NoseTextureGeneSet.Texture).DeepCopy;
    Nose.Colorize(NoseColorGeneSet.Color);
  end;
  Result := FTexture;
end;

{get those from Decoherence!}
type
  TBodyPart = class(TCastleTransform)
    procedure InjectTexture(const ATexture: TCastleImage);
  end; 