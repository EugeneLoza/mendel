unit form;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls,


  GeneticsGenome,
  GeneticsGenomeTemplateCore,
  GeneticsGenomeTemplates,
  GeneticsCore,
  GeneticsGeneCore,
  GeneticsGeneSet,
  GeneticsGeneSlotCore,
  GeneticsGeneSlots,

  CatGenes
  ;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    procedure Button1Click(Sender: TObject);
  private

  public

  end;

var
  Form1: TForm1;

implementation
uses
  Generics.Collections,
  CastleRandom;

{$R *.lfm}

procedure TForm1.Button1Click(Sender: TObject);
  function MakeSoftCopy(AGenericList: TGeneList): TGeneList;
  var
    I: Integer;
  begin
    Result := TGeneList.Create(false);
    for I := 0 to Pred(AGenericList.Count) do
      Result.Add(AGenericList[I]);
  end;
var
  BodyTemplate: TCatGenomeTemplate;
  GFF, GMF, GFM, GMM, Father, Mother, Child: TGenome;
  GenePoll: TGenePoll;
  GeneList: TGeneList;
  I: Integer;
begin
  Memo1.Clear;

  BodyTemplate := TCatGenomeTemplate.Create;

  GenePoll := TGenePoll.Create([doOwnsValues]);

  GeneList := TGeneList.Create(true);
  GeneList.Add(CreateGene(BodyTemplate.GenderGeneSlot, gtMale, 1));
  GeneList.Add(CreateGene(BodyTemplate.GenderGeneSlot, gtFemale, 1));
  GenePoll.Add('Gender', GeneList);

  GeneList := TGeneList.Create(true);
  for I := 0 to 18 do
    GeneList.Add(BodyTemplate.LeftEyeColorGeneSlot.RandomGene);
  GenePoll.Add('LeftEyeColor', GeneList);

  GeneList := TGeneList.Create(true);
  for I := 0 to 18 do
    GeneList.Add(BodyTemplate.RightEyeColorGeneSlot.RandomGene);
  GenePoll.Add('RightEyeColor', GeneList);

  GeneList := TGeneList.Create(true);
  for I := 0 to 18 do
    GeneList.Add(BodyTemplate.EyeColorAssymetry.RandomGene);
  GenePoll.Add('Dichromia', GeneList);

  GeneList := TGeneList.Create(true);
  for I := 0 to 18 do
    GeneList.Add(BodyTemplate.TailLengthGeneSlot.RandomGene);
  GenePoll.Add('TailLength', GeneList);

  GFF := BodyTemplate.RandomMaleGenome(GenePoll);
  GFF.BodyName := 'Grandfather - F';
  GMF := BodyTemplate.RandomFemaleGenome(GenePoll);
  GMF.BodyName := 'Grandmother - F';
  GFM := BodyTemplate.RandomMaleGenome(GenePoll);
  GFM.BodyName := 'Grandfather - M';
  GMM := BodyTemplate.RandomFemaleGenome(GenePoll);
  GMM.BodyName := 'Grandmother - M';

  Father := BodyTemplate.MaleChildGenome([GFF, GMF]);
  Father.BodyName := 'Father';
  Mother := BodyTemplate.FemaleChildGenome([GFM, GMM]);
  Mother.BodyName := 'Mother';

  Child := BodyTemplate.ChildGenome([Father, Mother]);
  Child.BodyName := 'Child';

  Memo1.Lines.Add(GFF.DebugString);
  Memo1.Lines.Add(GFM.DebugString);
  Memo1.Lines.Add(GMF.DebugString);
  Memo1.Lines.Add(GMM.DebugString);
  Memo1.Lines.Add(Father.DebugString);
  Memo1.Lines.Add(Mother.DebugString);
  Memo1.Lines.Add(Child.DebugString);

  Memo1.Lines.Add('Father true eye colors = (' +
    BodyTemplate.SayLeftEyeColor(Father) + ',' +
    BodyTemplate.SayRightEyeColor(Father)
    + ')');
  Memo1.Lines.Add('Mother true eye colors = (' +
    BodyTemplate.SayLeftEyeColor(Mother) + ',' +
    BodyTemplate.SayRightEyeColor(Mother)
    + ')');
  Memo1.Lines.Add('Child true eye colors = (' +
    BodyTemplate.SayLeftEyeColor(Child) + ',' +
    BodyTemplate.SayRightEyeColor(Child)
    + ')');

  FreeAndNil(GFF);
  FreeAndNil(GMF);
  FreeAndNil(GFM);
  FreeAndNil(GMM);
  FreeAndNil(Father);
  FreeAndNil(Mother);
  FreeAndNil(Child);

  FreeAndNil(BodyTemplate);

  FreeAndNil(GenePoll);
end;

end.

